## Biosecurity Commons Test data

This repository contains test data used to validate bsc-functions.

The data files are best accessed via a direct public URL in the form

```
https://gitlab.com/api/v4/projects/ecocommons-australia%2fbiosecuritycommons-platform%2fbsc-test/repository/files/data%2fregion_falls.tif/raw
```